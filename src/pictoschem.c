#include "name.h"
#include "pictoschem.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

static void pictoschem_init_schem(pictoschem_t *pts) {
	msch_t *m = &pts->schem;
	memset(m, 0, sizeof(msch_t));

	m->width = pts->width;
	m->height = pts->height;
	// Maximum number of tiles
	msch_resize_tiles(m, pts->width * pts->height);

	msch_add_tag(m, "name", pts->name);

	msch_add_block(m, "sorter");
}

static void pictoschem_calc_sorters(pictoschem_t *pts) {
	msch_t *m = &pts->schem;
	for (int y = 0; y < pts->height; y++) {
		colour_t *prow = &pts->pixels[y * pts->width];
		for (int x = 0; x < pts->width; x++) {
			if (prow[x].a >= pts->threshold) {
				msch_add_tile(m, (tile_t) {
					.x = x, .y = pts->height - y,
					.config = pictoschem_sorter_config(prow[x])
					// block is 0 - sorter
				});
			}
		}
	}
}

static int safestrcmp(char *a, char *b) {
	if (!a || !b) return 1;
	return strcmp(a, b);
}

/* Public functions */

void pictoschem_free(pictoschem_t *pts) {
	/* Close non-stdin/out files */
	if (pts->infile && pts->inpath) {
		fclose(pts->infile);
	}
	if (pts->outfile && pts->outpath) {
		fclose(pts->outfile);
	}

	if (pts->pixels) free(pts->pixels);
	msch_free(&pts->schem);
}

void pictoschem_die(pictoschem_t *pts, int code) {
	pictoschem_free(pts);
	exit(code);
}

void pictoschem_init(pictoschem_t *pts) {
	if (!safestrcmp(pts->inpath, pts->outpath)) {
		fprintf(stderr, "Input and output files cannot be the same.\n");
		pictoschem_die(pts, 1);
	}
	/* Open the files */
	pts->infile = pts->inpath ? fopen(pts->inpath, "rb") : stdin;
	pts->outfile = pts->outpath ? fopen(pts->outpath, "wb") : stdout;

	/* Error checking */
	if (!pts->infile) {
		fprintf(stderr, "Failed to open '%s'", pts->inpath);
		perror(" for reading");
		pictoschem_die(pts, errno);
	}

	if (!pts->outfile) {
		fprintf(stderr, "Failed to open '%s'", pts->outpath);
		perror(" for writing");
		pictoschem_die(pts, errno);
	}

	/* Set default name */
	if (!pts->name) {
		pts->name = pts->inpath ? pictoschem_getname(pts->inpath) : "stdin";
	}
}

void pictoschem_read(pictoschem_t *pts) {
	pts->image = (png_image) {
		.version = PNG_IMAGE_VERSION,
		.opaque = NULL
	};

	/* Read the header */
	if (!png_image_begin_read_from_stdio(&pts->image, pts->infile)) {
		fprintf(stderr, "Failed to begin reading image: %s\n", pts->image.message);
		pictoschem_die(pts, -1);
	}
	pts->width = pts->image.width;
	pts->height = pts->image.height;
	pts->image.format = PNG_FORMAT_RGBA;

	/* Allocate memory */
	pts->pixels = malloc(PNG_IMAGE_SIZE(pts->image));
	if (!pts->pixels) {
		png_image_free(&pts->image);
		perror("Failed to allocate memory for pixels");
		pictoschem_die(pts, errno);
	}

	if (!png_image_finish_read(&pts->image, NULL, pts->pixels, 0, NULL)) {
		fprintf(stderr, "Failed to read image: %s\n", pts->image.message);
		pictoschem_die(pts, -1);
	}

	pictoschem_init_schem(pts);

	pictoschem_calc_sorters(pts);
}

void pictoschem_write(pictoschem_t *pts) {
	msch_write(&pts->schem, pts->outfile);
	pictoschem_free(pts);
}
