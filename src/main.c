#include "pictoschem.h"

#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHECK(cond) if ((code = cond)) return code

/* Use stderr as to not write to a pipe expecting a schematic */
static void print_help() {
	fprintf(stderr, "Valid options:\n" \
		"\t-i <input file>: PNG to read, default = stdin\n" \
		"\t-o <output file>: Schem to write, default = stdout\n" \
		"\t-n <schem name>: Override the name, which is \"stdin\" or the\n" \
		"\t   input file without an extension.\n" \
		"\t-t <0-255> = 127: Set maximum transparency, pixels below this are ignored.\n");
}

int main(int argc, char **argv) {
	pictoschem_t pts;
	int opt;
	memset(&pts, 0, sizeof(pts));

	pts.threshold = 127;

	/* Parse options */
	while ((opt = getopt(argc, argv, "i:o:n:t:h")) != -1) {
		switch (opt) {
		case 'i':
			pts.inpath = optarg;
			break;
		case 'o':
			pts.outpath = optarg;
			break;
		case 'n':
			pts.name = optarg;
			break;
		case 't':
			errno = 0;
			pts.threshold = atoi(optarg);
			if (errno) {
				fprintf(stderr, "Invalid transparency threshold\n");
				return errno;
			}
			break;
		case 'h':
			print_help();
			return 0;
		/* Invalid arg */
		default:
			print_help();
			return 1;
		}
	}

	pictoschem_init(&pts);
	pictoschem_read(&pts);
	pictoschem_write(&pts);
	return 0;
}
