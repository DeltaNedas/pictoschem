#pragma once

#include "colours.h"

#include <mindustry/msch.h>
#include <png.h>

typedef struct {
	/* Options */
	char *inpath, *outpath, *name;
	// Pixels below this opacity are ignored
	char threshold;

	/* Image data */
	png_image image;
	colour_t *pixels;
	int width, height;

	/* Files */
	FILE *infile, *outfile;
	msch_t schem;
} pictoschem_t;

void pictoschem_free(pictoschem_t *pts);
void pictoschem_die(pictoschem_t *pts, int code);

// Open the files or stdin/out for NULL paths
void pictoschem_init(pictoschem_t *pts);
// Read the image from infile
void pictoschem_read(pictoschem_t *pts);
// Write the schematic to outfile
void pictoschem_write(pictoschem_t *pts);
