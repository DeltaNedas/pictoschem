#pragma once

typedef struct {
	unsigned char r, g, b, a;
} colour_t;

/* Get the closest sorter item for a given pixel */
int pictoschem_sorter_config(colour_t pixel);
